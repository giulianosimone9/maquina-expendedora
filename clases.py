import functions
import copy
import json

class Producto:
    def __init__(self,lote):
        self._lote=lote
        self._sig=None
        pass

    def getLote(self):return self._lote
    def getSig(self):return self._sig
    def setSig(self,sig):self._sig = sig; pass
  
class Nodo:
    def __init__(self,codigo,nombre,precio) -> None:
        self._codigo=codigo
        self._nombre=nombre
        self._precio=precio
        self._stockPila=None
        self._derecha=None
        self._izquierda=None
        pass
    
    #Getters
    def getCodigo(self):
        return self._codigo

    def getNombre(self):
        return self._nombre

    def getPrecio(self):
        return self._precio

    def getStockPila(self):
        return self._stockPila

    def setStockPila(self,stockPila):
        self._stockPila=stockPila

    def getDerecha(self):
        return self._derecha

    def setDerecha(self,nodo):
        self._derecha=nodo
        pass

    def getIzquierda(self):
        return self._izquierda
    
    def setIzquierda(self,nodo):
        self._izquierda=nodo
        pass
    
    def contarStock(self):
        if self._stockPila is None:
            return 0
        aux=self.desapilar()
        cont=self.contarStock()
        self.apilarStock(aux)
        return 1+cont


    def buscarPorCodigo(self,codigo):
        if self._codigo==codigo:
            return self
        elif self._codigo>codigo:
            if(self._izquierda is None):
                return None
            else:
                return self._izquierda.buscarPorCodigo(codigo)
        else:
            if(self._derecha is None):
                return None
            else:
                return self._derecha.buscarPorCodigo(codigo)
        
    def mostrar(self):
        if(self._izquierda is not None):
            self._izquierda.mostrar()
        print(self._nombre+" #"+str(self._codigo)+"\nPrecio: "+str(self._precio)+"\nStock: "+str(self.contarStock())+"\n\n")
        if(self._derecha is not None):
            self._derecha.mostrar()
    def stockVacio(self):
        return self._stockPila is None
    
    def apilarStock(self,lote):
        producto=Producto(lote)
        if self.stockVacio():
            self._stockPila=producto
        else:
            producto.setSig(self._stockPila)
            self._stockPila=producto
        return

    def desapilar(self)->Producto:
        if self.stockVacio():
            print("Producto sin stock")
            return None
        producto=self._stockPila
        self._stockPila=producto.getSig()
        return producto.getLote()

    def agregarProducto(self):
        print("Ingresar Stock del producto: ")
        while True:
            lote=input("Ingrese el codigo de Lote: ")
            self.apilarStock(lote)
            if(not functions.pedirConfirmacion("Desea seguir ingresando stock? Si / No: ")):
                break
    @staticmethod    
    def crear(codigo):
        nombre=input("Ingrese el nombre del producto: ")
        precio=functions.pedirFloat("Ingrese el precio del producto: ")
        return Nodo(codigo,nombre,precio)
        
class Arbol:
    def __init__(self):
        self._raiz=None
        pass

    def toJSON(self):
            #OBJECT TO JSON
        dictionary=copy.copy(self._raiz)
        dictionary=functions.dictionarizarRaiz(dictionary)
        json_object=json.dumps(dictionary, indent=4)
        with open("productos.json","w") as outfile:
            outfile.write(json_object)

    def mostrar(self):
        self._raiz.mostrar()
    
    def agregarRama(self,codigo):
        if self._raiz is None:
            self._raiz=Nodo.crear(codigo)
            self._raiz.agregarProducto()
        else:
            self._agregarRamaNodo(codigo,self._raiz)
        pass
    
    def _agregarRamaNodo(self,codigo,nodo):
        if nodo.getCodigo()>codigo:
                if nodo.getIzquierda() is not None:
                    self._agregarRamaNodo(codigo,nodo.getIzquierda())
                else:
                    nodo.setIzquierda(Nodo.crear(codigo))
                    nodo.getIzquierda().agregarProducto()
        elif nodo.getCodigo()<codigo:
            if nodo.getDerecha() is not None:
                self._agregarRamaNodo(codigo,nodo.getDerecha())
            else:
                nodo.setDerecha(Nodo.crear(codigo))
                nodo.getDerecha().agregarProducto()            
        else:
            add=functions.pedirConfirmacion("Codigo ya existente, desea agregar stock? Si / No: ")
            if(add):
                lote=input("Ingrese el codigo de Lote: ")
                nodo.apilarStock(lote)
        return

    def buscarPorCodigo(self, codigo):
        return self._raiz.buscarPorCodigo(codigo)