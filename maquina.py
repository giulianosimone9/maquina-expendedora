import functions


class Maquina:
    def __init__(self,productos,password):
        self._productos=productos
        self._prendida=True
        self._saldo=0
        self._password=password
    
    def apagar(self):
        password=input("Ingrese su contraseña: ")
        if password==self._password:
            self._prendida=False
        else:
            print("Contraseña incorrecta")

    def agregarSaldo(self,saldo):
        self._saldo+=saldo
    
    def devolverSaldo(self):
        devolver=self._saldo
        self._saldo=0
        return devolver
    
    def venderProducto(self,codigo):
        producto=self._productos.buscarPorCodigo(codigo)
        if producto is None:
            print("Codigo inexistente")
            return None
    
        if producto.getStockPila() is None:
            print("Lo sentimos! No hay stock para este producto")
            return None
        while producto.getPrecio() > self._saldo:
            print(f"Saldo actual insuficiente (${self._saldo})")
            if not functions.pedirConfirmacion("Quiere agregar saldo?: "):
                return
            print(f"Faltan ${producto.getPrecio()-self._saldo}")
            saldo=functions.pedirFloat("Ingrese el monto a añadir: ")
            self.agregarSaldo(saldo)
            
        print("Disfrute su "+producto.getNombre()+"\nSu vuelto es de: $"+str(self._saldo-producto.getPrecio()))
        self._saldo=0
        return producto.desapilar()
    
    def prender(self):
        while self._prendida:
            functions.clear()
            self._productos.mostrar()
            opcion=input("Bienvenido, que desea hacer?\nSaldo Actual: ($"+str(self._saldo)+")\n\t1-Elegir producto\n\t2-Agregar Saldo\n\t3-Cancelar\nElige una opcion: ")
            functions.clear()
            match opcion:
                case "1":  
                    functions.clear()
                    print("Listado de productos")
                    self._productos.mostrar()
                    codigo=functions.pedirInt("Ingrese el codigo: ")
                    self.venderProducto(codigo)
                case "2":
                    saldo=functions.pedirFloat("Ingrese el monto: ")
                    self.agregarSaldo(saldo)
                case "3":
                    print("Adios, le devolvemos $"+str(self.devolverSaldo()))
                case "apagar":
                    print("Como diga jefe")
                    self.apagar()
                case other:
                    print("Comando invalido")
            
            input("Presione Enter para continuar.")
        pass