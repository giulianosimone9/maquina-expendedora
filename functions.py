from os import system, name
import copy

def clear():
 
    # Windows
    if name == 'nt':
        system('cls')
 
    #Mac y Linux
    else:
        system('clear')

def pedirConfirmacion(text):
    while True:
        add=input(text)
        add=add.lower()
        if(add in ["s","si","yes","y","1","n","no","0"]):
            return add in ["s","si","yes","y","1"]
        else:
            print("Comando invalido")

def crearProductos(productos):
    print("Bienvenido a la maquina expendedora\nToca ingresar Stock")
    while True: 
        codigo=pedirInt("Ingrese el codigo del producto: ")
        productos.agregarRama(codigo)
        add=pedirConfirmacion("Desea agregar otro producto? Si / No: ")
        if(not add):
            break

    productos.toJSON()
    return productos

def pedirInt(text):
    while True:
        try:
            numero=int(input(text))
            break
        except ValueError:
            print("Ingrese un numero valido")
    return numero

def pedirFloat(text):
    while True:
        try:
            numero=float(input(text))
            break
        except ValueError:
            print("Ingrese un numero valido")
    return numero


def dictionarizarRaiz(dictionary):
    copied=copy.copy(dictionary)
    if(copied is None):
        return
    stock=[]
    while not copied.stockVacio():
        stock.append({"lote":copied.desapilar()})
    
    copied.setStockPila(stock)
    if(copied.getIzquierda() is not None):
        copied.setIzquierda(dictionarizarRaiz(copied.getIzquierda()))
    if(copied.getDerecha() is not None):
        copied.setDerecha(dictionarizarRaiz(copied.getDerecha()))
    return copied.__dict__

